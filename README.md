![Squid](./resources/logotipo_squid.png)
# Your multihanded companion

[![codecov](https://codecov.io/gl/entel-ocean/squid/branch/master/graph/badge.svg?token=pStzopYsx2)](https://codecov.io/gl/entel-ocean/squid)
[![coverage report](https://gitlab.com/entel-ocean/squid/badges/master/coverage.svg)](https://gitlab.com/entel-ocean/squid/-/commits/master)
[![pipeline status](https://gitlab.com/entel-ocean/squid/badges/master/pipeline.svg)](https://gitlab.com/entel-ocean/squid/-/commits/master)
[![StackShare](http://img.shields.io/badge/tech-stack-0690fa.svg?style=flat)](https://stackshare.io/entelocean/squidci)
[![CodeInspector quality](https://www.code-inspector.com/project/7935/score/svg)](https://frontend.code-inspector.com/public/project/7935/squid/dashboard)
[![CodeInspector grade](https://www.code-inspector.com/project/7935/status/svg)](https://frontend.code-inspector.com/public/project/7935/squid/dashboard)
[![time tracker](https://wakatime.com/badge/gitlab/entel-ocean/squid.svg)](https://wakatime.com/badge/gitlab/entel-ocean/squid)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

## Description
Orchestration tool for managing resources and actions on git monorepos

## Commands on dev
* Build package `poetry build`
* Run console cli `poetry run squid --help`
* Run tests `poetry nox -s tests`

Details on testing **[coverage](http://squid.entelocean.io/coverage/)**.

## Squid Framework
The Squid Framework is integrated mainly by:
* Tentacles which are the resource that compose your monorepo aka directories
* Tasks which are specialized action you want to run inside your resources aka Tentacles
* Plugins which are python modules that extends the squid framework
* Commands which are the actions you can perform to interact with squid such as:
  * **INIT** -> to initialize squid project.
  * **ADD** -> to add tentacles, tasks or plugins
  * **GET** -> to query tentacles, tasks or plugins
  * **UPDATE** -> to modify already existing tentacles, tasks or plugins
  * **REMOVE** -> to delete tentacles, tasks or plugins
  * **LINK** -> to link tentacles to tasks, tentacles to tentacles, tasks to tasks creating a DAG
  * **EXECUTE** -> to execute tasks on tentacles already linked to the tentacle or defined dynamically
  * **INVOKE** -> to call functions defined on plugins or to create a execution context
  * **DUMP** -> to serialized and persists config of Tentacles or Plugins
* Services which are the backbone of squid compose by:
  * **DB** -> it is a db engine for all CRUD operations perform by commands
  * **ENGINE** -> it is the DAG execution engine for the tasks
  * **PERSISTENCE** -> it manages the interaction with the file system
  * **REGISTRY** -> it manages the interaction with the plugins

More details on **[API Reference](http://squid.entelocean.io/reference/)**.

## Squid Interfaces
These are the interfaces on which the user can interact with the squid framework
* CONSOLE CLI
* RESTAPI

### Usage of the CLI
**[BOOK](http://squid.entelocean.io/book/)** - _This is based on: [mdbook](https://rust-lang.github.io/mdBook/) / [termbook](https://byron.github.io/termbook/introduction.html) and all examples can be run with `termbook play`_.

### Usage of the REST API
**TBD**

### Inspirations
The query syntax, output filter and execution engine are pretty much a copy from this three awesome sources
* Query syntax and operations: [TinyDB](https://tinydb.readthedocs.io/en/latest/usage.html)
* Filter: [JsonPath](https://goessner.net/articles/JsonPath/) and [JsonPath RW Ext](https://python-jsonpath-rw-ext.readthedocs.io/en/latest/readme.html#quick-start). You can use [this](https://jsonpath.com/) to test syntax.
* Execution engine: [Luigi](https://github.com/spotify/luigi)


### TODO
* Create setting on init for use other db and execution engines (now it uses DP on the service layer but on the commands it is hardcoded)
* Evaluate multiple Receivers for the command layer right now it is only Squid class
* Use dumps on MemoryStorage of DB to simulate better dry-runs.
* Use progress bar on execute.
* Better tests cases there are few that are couple with implementation and not with mocks.
* Better docs pages.
