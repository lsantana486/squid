FROM python:3.8.3-alpine3.11

RUN apk add gcc python3-dev libc-dev libffi-dev openssl-dev && \
    mkdir -p /dist
COPY dist/ /dist
COPY requirements.txt /dist/requirements.txt

RUN pip install poetry toml && \
    pip install -r /dist/requirements.txt && \
    mv /dist/squid-*.tar.gz /dist/squid.tar.gz && \
    pip install /dist/squid.tar.gz

WORKDIR /mnt
ENTRYPOINT ["squid"]
