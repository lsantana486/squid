# MISC Action
This action grouped all the miscellaneous resources

## MISC Resources
* **hello**: get a salute from Squid
```bash
squid misc hello
```

* **saying**: get a quote from Squid
```bash,use=example,exec
squid misc saying
```
