# Actions
All CLI actions have the following characteristics:

* All have the option **--help** to get the usage guide
* All have the syntax `squid <ACTION> <RESOURCE> <OPTIONS>`
