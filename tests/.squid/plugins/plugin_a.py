# flake8: noqa
def make_secret(text: str):
    return "".join(["x" if not char.isspace() else char for char in text])


def setup(registry):
    registry.entry("secret", make_secret)
