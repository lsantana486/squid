"""Test Squid Models."""
from squid.framework.core.models.bases import (
    Link,
    LinkedCls,
    Plugin,
    SquidClass,
    Task,
    TaskCmd,
    Tentacle,
    TentacleRef,
)


def test_creation_tentacle_ref() -> None:
    """Test Tentacle Ref instance."""
    tentacle_ref = TentacleRef(name="test", type="test", path=".")
    assert isinstance(tentacle_ref, TentacleRef)


def test_creation_tentacle() -> None:
    """Test Tentacle instance."""
    tentacle = Tentacle(
        name="test",
        type="test",
        path=".",
        attributes=[{"deployer": "sls"}],
        tentacles=[],
        tasks=[],
    )
    assert isinstance(tentacle, Tentacle)


def test_creation_task() -> None:
    """Test Task Ref instance."""
    cmds = [TaskCmd(cmd="echo", args=["test"])]
    task = Task(name="test", category="echo", envs={}, cmds=cmds)
    assert isinstance(task, Task)


def test_creation_tentacle_w_tasks() -> None:
    """Test Tentacle with tasks instance."""
    cmds = [TaskCmd(cmd="echo", args=["test"])]
    task = Task(name="test", category="echo", envs={}, cmds=cmds)
    tentacle = Tentacle(
        name="test",
        type="test",
        path=".",
        attributes=[{"deployer": "sls"}],
        tentacles=[],
        tasks=[task],
    )
    assert isinstance(tentacle, Tentacle)


def test_creation_tentacle_w_task_tref() -> None:
    """Test Tentacle with task Ref instance."""
    cmds = [TaskCmd(cmd="echo", args=["test"])]
    task = Task(name="test", category="echo", envs={}, cmds=cmds)
    tentacle_ref = Tentacle(name="test", type="test", path=".", tentacles=[], tasks=[])
    tentacle = Tentacle(
        name="test",
        type="test",
        path=".",
        attributes=[{"deployer": "sls"}],
        tentacles=[tentacle_ref],
        tasks=[task],
    )
    assert isinstance(tentacle, Tentacle)


def test_creation_tentacle_w_task_tentacle() -> None:
    """Test Tentacle with Tentacle and task Ref instance."""
    cmds = [TaskCmd(cmd="echo", args=["test"])]
    task = Task(name="test", category="echo", envs={}, cmds=cmds)
    tentacle_a = Tentacle(
        name="testa",
        type="test",
        path=".",
        attributes=[{"deployer": "sls"}],
        tentacles=[],
        tasks=[task],
    )
    tentacle = Tentacle(
        name="test",
        type="test",
        path=".",
        attributes=[{"deployer": "sls"}],
        tentacles=[tentacle_a],
        tasks=[],
    )
    assert isinstance(tentacle, Tentacle)


def test_creation_plugin() -> None:
    """Test Plugin Ref instance."""
    plugin = Plugin(name="test", source=".", entrypoint="test:main")
    assert isinstance(plugin, Plugin)


def test_creation_link() -> None:
    """Test Link instance."""
    link = Link(
        name="tentacle_a",
        cls=SquidClass.TENTACLE,
        dependencies=[LinkedCls(name="task_a", cls=SquidClass.TASK)],
        dependant=[],
    )
    assert isinstance(link, Link)
