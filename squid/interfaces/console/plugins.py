"""Add commands for plugins cls CLI."""
import functools
import inspect
from typing import Any, Callable, List, Optional

import click
import typer

from squid.framework.core import GetRegistryEntries, InvokeFn, Squid
from squid.framework.utils import to_str
from squid.interfaces.console import SquidCLI


fn_set_option: Callable[
    [Callable, str, inspect.Parameter], Callable
] = lambda fn, param_name, param_cfg: click.option(
    f"--{param_name}",
    required=param_cfg.default == inspect._empty,  # type: ignore
    type=param_cfg.annotation,
    default=None if param_cfg.default == inspect._empty else param_cfg.default,  # type: ignore
)(
    fn
)

fn_set_parameter: Callable[
    [Callable, str], Callable
] = lambda fn, param_name: click.argument(param_name, nargs=-1, type=str)(fn)


def fn_wrapper(fn: Callable) -> Callable:
    """Wrapper for calling fn."""

    @functools.wraps(fn)
    def wrapper(*args, **kwargs) -> None:
        try:
            fn_response = fn(*args, **kwargs)
            typer.secho(to_str(fn_response), fg=typer.colors.GREEN)
        except Exception as e:
            typer.echo(e, err=True)

    return wrapper


def transform_function_to_command(fn: Callable) -> click.Command:
    """Transform fn to click command."""
    fn_wrapped = fn_wrapper(fn)
    fn_to_invoke = fn_wrapped
    flag_ignore_unknown_options = False
    for param_name, param_cfg in inspect.signature(fn).parameters.items():
        if param_cfg.annotation is list or param_cfg.annotation is tuple:
            flag_ignore_unknown_options = True
            fn_to_invoke = fn_set_parameter(fn_to_invoke, param_name)  # type: ignore
        else:
            fn_to_invoke = fn_set_option(fn_to_invoke, param_name, param_cfg)  # type: ignore
    return click.command(
        context_settings={"ignore_unknown_options": flag_ignore_unknown_options}
    )(fn_to_invoke)


def get_entries_plugins_cli() -> List[str]:
    """Get entries commands."""
    squid_cli = SquidCLI()
    squid = Squid()
    return [entry for entry in squid_cli.execute(GetRegistryEntries(squid))]


def get_plugin_command(name: str) -> click.Command:
    """Get plugin commands."""
    squid_cli = SquidCLI()
    squid = Squid()
    return transform_function_to_command(squid_cli.execute(InvokeFn(squid, name)))


class SquidPluginsGroup(click.Group):
    """Custom click group to use with plugins dynamic."""

    def get_command(self, ctx: click.Context, cmd_name: str) -> Optional[Any]:
        """Get command from plugins."""
        if cmd_name in get_entries_plugins_cli():
            self.add_command(get_plugin_command(cmd_name), cmd_name)
        return super(SquidPluginsGroup, self).get_command(ctx, cmd_name)

    def list_commands(self, ctx: click.Context) -> List:
        """List command from plugins."""
        for entry in get_entries_plugins_cli():
            self.add_command(click.Command(name=entry), entry)
        return super(SquidPluginsGroup, self).list_commands(ctx)  # type: ignore


@click.group(cls=SquidPluginsGroup)
def cli() -> None:
    """Main command for this group."""
    pass
