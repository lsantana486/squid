"""Update tentacles|tasks|plugins."""
import os
from typing import cast

import typer

from squid.framework.core import (
    PersistenceResponsePath,
    Plugin,
    Squid,
    SquidPersistence,
    Task,
    Tentacle,
)
from squid.framework.core.commands.add import AddPlugins, AddTasks, AddTentacles
from squid.framework.core.commands.get import GetPlugins, GetTasks, GetTentacles
from squid.framework.core.commands.update import (
    UpdatePlugins,
    UpdateTasks,
    UpdateTentacles,
)
from squid.framework.utils import to_obj
from squid.interfaces.console import SquidCLI
from squid.interfaces.console.callbacks import (
    cli_callback,
    editor_callback,
    regular_callback,
)


cli = typer.Typer(
    callback=cli_callback,
    invoke_without_command=True,
    help="Update Tentacles | Tasks | Plugins",
)


@cli.command(name="tentacles")
def update_tentacles(
    kvs: str = typer.Option(None, callback=regular_callback),  # noqa: B008
    query: str = typer.Option(  # noqa: B008
        ..., "--query", "-q", callback=regular_callback
    ),
    editor: bool = typer.Option(  # noqa: B008
        None,
        "--editor",
        "-e",
        callback=editor_callback,
        help="Flag to use the default text editor of the system.",
    ),
    verbose: bool = typer.Option(None, "--verbose", "-v"),  # noqa: B008
    dry_run: bool = typer.Option(None, "--dry-run", "-d"),  # noqa: B008
) -> None:
    """Update tentacles by CLI."""
    if verbose:
        typer.secho(
            "\n".join(
                [
                    f"{opt} = {value}"
                    for opt, value in [
                        ("kvs", kvs),
                        ("query", query),
                        ("editor", editor),
                    ]
                ]
            )
        )
    try:
        tentacles = SquidCLI().execute(
            GetTentacles(Squid(), query=query, output="tree")
        )
        if dry_run:
            typer.echo(
                f"Tentacles original: \n{[Tentacle.parse_obj(tentacle) for tentacle in tentacles]}"
            )
            typer.echo(f"Tentacles update: \n{editor or kvs}")
        else:
            if editor:
                cwd = os.curdir
                os.chdir(
                    cast(
                        PersistenceResponsePath, SquidPersistence().get_root_path().data
                    ).path
                )
                SquidCLI().execute(AddTentacles(Squid(), editor, True))  # type: ignore
                os.chdir(cwd)
            else:
                SquidCLI().execute(UpdateTentacles(Squid(), query, to_obj(kvs)))  # type: ignore
            typer.secho("Success!!!", fg=typer.colors.GREEN, bold=True)
    except Exception as e:
        typer.echo(e, err=True)


@cli.command(name="tasks")
def update_tasks(
    kvs: str = typer.Option(None, callback=regular_callback),  # noqa: B008
    query: str = typer.Option(  # noqa: B008
        ..., "--query", "-q", callback=regular_callback
    ),
    editor: bool = typer.Option(  # noqa: B008
        None,
        "--editor",
        "-e",
        callback=editor_callback,
        help="Flag to use the default text editor of the system.",
    ),
    verbose: bool = typer.Option(None, "--verbose", "-v"),  # noqa: B008
    dry_run: bool = typer.Option(None, "--dry-run", "-d"),  # noqa: B008
) -> None:
    """Update tasks by CLI."""
    if verbose:
        typer.secho(
            "\n".join(
                [
                    f"{opt} = {value}"
                    for opt, value in [
                        ("kvs", kvs),
                        ("query", query),
                        ("editor", editor),
                    ]
                ]
            )
        )
    try:
        tasks = SquidCLI().execute(GetTasks(Squid(), query=query, output="tree"))
        if dry_run:
            typer.echo(f"Tasks original: \n{[Task.parse_obj(task) for task in tasks]}")
            typer.echo(f"Tasks update: \n{editor or kvs}")
        else:
            if editor:
                SquidCLI().execute(AddTasks(Squid(), editor, True))  # type: ignore
            else:
                SquidCLI().execute(UpdateTasks(Squid(), query, to_obj(kvs)))  # type: ignore
            typer.secho("Success!!!", fg=typer.colors.GREEN, bold=True)
    except Exception as e:
        typer.echo(e, err=True)


@cli.command(name="plugins")
def update_plugins(
    kvs: str = typer.Option(None, callback=regular_callback),  # noqa: B008
    query: str = typer.Option(  # noqa: B008
        ..., "--query", "-q", callback=regular_callback
    ),
    editor: bool = typer.Option(  # noqa: B008
        None,
        "--editor",
        "-e",
        callback=editor_callback,
        help="Flag to use the default text editor of the system.",
    ),
    verbose: bool = typer.Option(None, "--verbose", "-v"),  # noqa: B008
    dry_run: bool = typer.Option(None, "--dry-run", "-d"),  # noqa: B008
) -> None:
    """Update plugins by CLI."""
    if verbose:
        typer.secho(
            "\n".join(
                [
                    f"{opt} = {value}"
                    for opt, value in [
                        ("kvs", kvs),
                        ("query", query),
                        ("editor", editor),
                    ]
                ]
            )
        )
    try:
        plugins = SquidCLI().execute(GetPlugins(Squid(), query=query, output="tree"))
        if dry_run:
            typer.echo(
                f"Plugins original: \n{[Plugin.parse_obj(plugin) for plugin in plugins]}"
            )
            typer.echo(f"Plugins update: \n{editor or kvs}")
        else:
            if editor:
                cwd = os.curdir
                os.chdir(
                    cast(
                        PersistenceResponsePath, SquidPersistence().get_root_path().data
                    ).path
                )
                SquidCLI().execute(AddPlugins(Squid(), editor, True))  # type: ignore
                os.chdir(cwd)
            else:
                SquidCLI().execute(UpdatePlugins(Squid(), query, to_obj(kvs)))  # type: ignore
            typer.secho("Success!!!", fg=typer.colors.GREEN, bold=True)
    except Exception as e:
        typer.echo(e, err=True)
