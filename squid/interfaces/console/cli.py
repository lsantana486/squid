"""Interface CLI for Squid."""
import click
import pkg_resources
import sentry_sdk
import typer

import squid.interfaces.console.add as add
import squid.interfaces.console.execute as execute
import squid.interfaces.console.get as get
import squid.interfaces.console.init as init
import squid.interfaces.console.link as link
import squid.interfaces.console.misc as misc
import squid.interfaces.console.plugins as plugins
import squid.interfaces.console.remove as remove
import squid.interfaces.console.update as update

sentry_sdk.init(
    "https://XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX@XXXXXXX.ingest.sentry.io/0000000",
    release="#RELEASE#",
)

squid = typer.Typer()


@squid.command()
def version() -> None:
    """Get version of Squid."""
    squid_version = pkg_resources.get_distribution("squid").version
    typer.echo(f"Squid CLI Version: v{squid_version}")
    raise typer.Exit()


@click.group()
def squid_main() -> None:
    """Main group."""
    pass


squid_main.add_command(plugins.cli, "plugins")
squid_main.add_command(typer.main.get_command(squid), "version")
squid_main.add_command(typer.main.get_command(misc.cli), "misc")
squid_main.add_command(typer.main.get_command(execute.cli), "execute")
squid_main.add_command(typer.main.get_command(add.cli), "add")
squid_main.add_command(typer.main.get_command(get.cli), "get")
squid_main.add_command(typer.main.get_command(init.cli), "init")
squid_main.add_command(typer.main.get_command(link.cli), "link")
squid_main.add_command(typer.main.get_command(remove.cli), "remove")
squid_main.add_command(typer.main.get_command(update.cli), "update")

if __name__ == "__main__":
    squid_main()
