"""Create Invoker for CLI."""
from enum import Enum

from squid.framework.core import (
    AddPlugins,
    AddTasks,
    AddTentacles,
    CreateLink,
    Execute,
    GetPlugin,
    GetPlugins,
    GetRegistryEntries,
    GetTask,
    GetTasks,
    GetTentacle,
    GetTentacles,
    Init,
    InvokeCtx,
    InvokeFn,
    Invoker,
    RemoveLink,
    RemovePlugins,
    RemoveTasks,
    RemoveTentacles,
    UpdatePlugins,
    UpdateTasks,
    UpdateTentacles,
)


class SquidCLI(Invoker):
    """Invoker implementation."""

    def __init__(self) -> None:
        """Constructor."""
        super().__init__(
            [
                Execute,
                CreateLink,
                Init,
                InvokeCtx,
                InvokeFn,
                AddPlugins,
                AddTasks,
                AddTentacles,
                GetPlugin,
                GetPlugins,
                GetRegistryEntries,
                GetTask,
                GetTasks,
                GetTentacle,
                GetTentacles,
                RemoveLink,
                RemovePlugins,
                RemoveTasks,
                RemoveTentacles,
                UpdatePlugins,
                UpdateTasks,
                UpdateTentacles,
            ]
        )


class CliOutput(str, Enum):
    """CliOutput Model."""

    YAML = "yaml"
    JSON = "json"
