"""Miscelaneos commands."""
# flake8: noqa
import requests
import typer

cli = typer.Typer()


@cli.command()
def hello() -> None:
    """Hello from Squid."""
    squid_ascii = """
                                             ####
                                           ##    ##
                                         ###      ##
                                        ##         ###
                                       ##            ##
                                      ##              ##
                                     ##                ##
                                    ##                  ##
                                    ##                  ##
                                     ##                 ##
                                      ##               ##
                                       ##             ##
                                      ##              ##
                                     ##                ##
                                    ##                  ##
                                    ##                  ##
                                   ##                    ##
                                   ##                    ##
                                  ##                      ##
                                  ##                      ##
                                 ##                        ##
                                 ##                        ##
        #####                    ##                         ##                     #####
     ###     ###                ##                          ##                  ###    ####
    ##        ###               ##            ##            ##                 ##         ##
   ##          ##               ##            ###            ##               ##          ##
   ##           ##             ##        ###  ###  ###       ##               ##           ##
 ######         ##             ##      ###    ###    ###     ##               ##         ######
##   ##         ##             ##     ##      ###     ##      ##              ##        ##   ##
 ######         ##              ##    ##      ###      ##    ##               ##         ######
                ##              ##    ##      ###      ##    ##               ##
                ##               ##    ##             ##    ###               ##
                ##                ##    ##          ###    ###                ##
                ##              ######    ###########     #####              ##
                 ##          ####    ###               ###     ###           ##
                  ###   ######        ####################        #####   ####
 #####               ####            ##    ##     ##    ##             ###               ######
##   #####                          ##     ##     ##     ##                          #####   ##
 ######   ####                      ##     ##     ##      ##                      ####   ######
             ####                  ##      ##     ##      ##                  ####
                 ####             ##       ##     ##       ##              ####
                    #####        ##        ##     ##        ###       #####
                         ########          ##     ##          ########
                                           ##     ##
                                 ##        ##     ##        ###
                               ##  ##      ##     ##      ### ###
                               #   ##      ##     ##      ##   ##
                                ####       ##     ##       #####
                                 ##        ##      ##       ##
                                  ##      ##       ###     ##
                                    ######           #######
    """
    typer.secho(squid_ascii, fg=typer.colors.GREEN, bold=True)


@cli.command()
def saying() -> None:
    """Let's get what Squid is thinking."""
    msg = "Well, I guess I ran of sayings..."
    quote_message = {"quote": msg}
    response = requests.get("https://quotes.rest/qod", params={"category": "funny"})
    if response.status_code == requests.codes.ok:
        quote_json = response.json()
        if "success" in quote_json and quote_json["success"].get("total", 0) > 0:
            quote_message = (
                quote_json.get("contents", {}).get("quotes", [quote_message]).pop()
            )
    elif response.status_code == 429:
        quote_json = response.json()
        error_message = (
            quote_json.get("error", {})
            .get("message", "")
            .replace(
                "Too Many Requests: Rate limit of 10 requests per hour exceeded. Please wait for",
                "so give me",
            )
            .strip(".")
        )
        quote_message["quote"] = f"{msg} {error_message} to think..."
    typer.echo(
        typer.style(quote_message.get("quote", msg), fg=typer.colors.GREEN, bold=True)
    )


if __name__ == "__main__":
    cli()
