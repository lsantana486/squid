"""Service to manage persistence on Files."""
import json
import os
from pathlib import Path
import re
import shutil
from typing import Any, Callable, cast, List, Union

import yaml

from squid.framework.core.models.bases import Plugin, Tentacle, TentacleRef
from squid.framework.core.models.responses import (
    PersistenceResponsePath,
    ServiceResponse,
    StatusEnum,
)
from squid.framework.services import SingletonService
from squid.framework.utils import SquidDumper


class SquidPersistence(metaclass=SingletonService):
    """Singleton Entity."""

    _squid_path_match_pattern: str
    _tentacle_path_match_pattern: str

    def __init__(
        self,
        squid_path_match_pattern: str = "^.squid$",
        tentacle_path_match_pattern: str = "^.tentacle$",
    ) -> None:
        """Obtain singleton Instance.

        Args:
            squid_path_match_pattern: str
            tentacle_path_match_pattern: str

        """
        self._squid_path_match_pattern = squid_path_match_pattern
        self._tentacle_path_match_pattern = tentacle_path_match_pattern

    @staticmethod
    def _find_by_pattern_from_path(current_path: str, pattern: str) -> str:
        """Find a directory path by a pattern from the current directory.

        Args:
            current_path: str
            pattern: str

        Returns:
            str

        Rasises:
            Exception: Can't find path from <current_path> with pattern <pattern>. # noqa: DAR401

        """
        result = None
        pattern_re = re.compile(pattern)
        cp_current_path = current_path
        while True:
            file_names = os.listdir(current_path)
            project_root_files = list(filter(pattern_re.match, file_names))
            if project_root_files:
                result = current_path
                break

            if current_path == "/":
                break

            current_path = os.path.abspath(os.path.join(current_path, ".."))
        if result is None:
            raise Exception(
                f"Can't find path from {cp_current_path} with pattern {pattern}"
            )
        return result

    def get_root_path(self, current_path: str = None) -> ServiceResponse:
        """Get path for root folder.

        Args:
            current_path: str

        Returns:
            ServiceResponse

        """
        current_path = current_path or os.getcwd()
        current_fullpath = os.path.abspath(
            os.path.normpath(os.path.expanduser(current_path))
        )
        if not os.path.isdir(current_fullpath):
            current_fullpath = os.path.dirname(current_fullpath)
        return ServiceResponse(
            status=StatusEnum.success,
            data=PersistenceResponsePath(
                path=SquidPersistence._find_by_pattern_from_path(
                    current_path=current_fullpath,
                    pattern=self._squid_path_match_pattern,
                )
            ),
        )

    def get_squid_path(self, current_path: str = None) -> ServiceResponse:
        """Get path for .squid folder.

        Args:
            current_path: str

        Returns:
            ServiceResponse

        """
        current_path = current_path or os.getcwd()
        current_fullpath = os.path.abspath(
            os.path.normpath(os.path.expanduser(current_path))
        )
        if not os.path.isdir(current_fullpath):
            current_fullpath = os.path.dirname(current_fullpath)
        return ServiceResponse(
            status=StatusEnum.success,
            data=PersistenceResponsePath(
                path=os.path.join(
                    SquidPersistence._find_by_pattern_from_path(
                        current_path=current_fullpath,
                        pattern=self._squid_path_match_pattern,
                    ),
                    ".squid",
                )
            ),
        )

    def get_tentacle_path(self, current_path: str = None) -> ServiceResponse:
        """Get path for .tentacle folder.

        Args:
            current_path: str

        Returns:
            str

        """
        current_path = current_path or os.getcwd()
        current_fullpath = os.path.abspath(
            os.path.normpath(os.path.expanduser(current_path))
        )
        if not os.path.isdir(current_fullpath):
            current_fullpath = os.path.dirname(current_fullpath)
        return ServiceResponse(
            status=StatusEnum.success,
            data=PersistenceResponsePath(
                path=os.path.join(
                    SquidPersistence._find_by_pattern_from_path(
                        current_path=current_fullpath,
                        pattern=self._tentacle_path_match_pattern,
                    ),
                    ".tentacle",
                )
            ),
        )

    @staticmethod
    def dump_to_file(
        item: dict, path: str, filename: str = "dump", fmt: str = "YAML"
    ) -> None:
        """Dump file with yaml or json format.

        Args:
            item: dict
            path: str
            filename: str = 'dump'
            fmt: str = 'YAML'

        Raises:
            Exception: Format is not supported try YAML or JSON

        """
        if not os.path.exists(path):
            raise Exception("Path does not exists")
        filename = f"{filename}.{fmt.lower()}"
        file_fullpath: str = os.path.join(path, filename)
        with open(file_fullpath, "w") as f:
            if fmt == "YAML":
                _ = yaml.dump(item, f, sort_keys=True, indent=4, Dumper=SquidDumper)
            elif fmt == "JSON":
                json.dump(item, f, sort_keys=True, indent=4)
            else:
                raise Exception("Format is not supported try YAML or JSON")

    def make_tentacle_path(self, tentacle: Union[Tentacle, TentacleRef]) -> str:
        """Create tentacle folder.

        Args:
            tentacle: Union[Tentacle, TentacleRef]

        Returns:
            str

        Raises:
            Exception: Tentacle path <real_path> does not exists.

        """
        root_path = cast(PersistenceResponsePath, self.get_root_path().data).path
        real_path = os.path.abspath(tentacle.path)
        if not os.path.exists(real_path):
            raise Exception(f"Tentacle path {real_path} does not exists.")
        tentacle_path = Path(real_path).joinpath(".tentacle")
        tentacle_path.mkdir(exist_ok=True)
        tentacle_logs = tentacle_path.joinpath("logs")
        tentacle_logs.mkdir(exist_ok=True)
        tentacle_logs.joinpath(".gitkeep").touch()
        return f"./{os.path.relpath(real_path, root_path)}"

    def remove_tentacle_path(self, tentacle: Union[Tentacle, TentacleRef]) -> None:
        """Remove tentacle folder.

        Args:
            tentacle: Union[Tentacle, TentacleRef]

        """
        root_path = cast(PersistenceResponsePath, self.get_root_path().data).path
        real_path = os.path.abspath(os.path.join(root_path, tentacle.path))
        tentacle_path = Path(real_path).joinpath(".tentacle")
        shutil.rmtree(str(tentacle_path.absolute()))

    def make_squid_plugin_path(self, plugin: Plugin) -> str:
        """Create squid_plugin folder.

        Args:
            plugin: Plugin

        Returns:
            str

        Raises:
            Exception: Plugin path <real_path> does not exists.

        """
        root_path = cast(PersistenceResponsePath, self.get_root_path().data).path
        real_path = os.path.abspath(plugin.path)
        if not os.path.exists(real_path):
            raise Exception(f"Plugin path {real_path} does not exists.")
        plugin_path = Path(real_path).joinpath(".squid-plugin")
        plugin_path.mkdir(exist_ok=True)
        plugin_path.joinpath(".gitkeep").touch()
        return f"./{os.path.relpath(real_path, root_path)}"

    def remove_squid_plugin_path(self, plugin: Plugin) -> None:
        """Remove squid_plugin folder.

        Args:
            plugin: Plugin

        """
        root_path = cast(PersistenceResponsePath, self.get_root_path().data).path
        real_path = os.path.abspath(os.path.join(root_path, plugin.path))
        plugin_path = Path(real_path).joinpath(".squid-plugin")
        shutil.rmtree(str(plugin_path.absolute()))

    def item_abspath(self, path: str) -> str:
        """Return abspath from relative path."""
        return os.path.abspath(
            os.path.join(
                cast(PersistenceResponsePath, self.get_root_path().data).path, path
            )
        )

    def set_abspath(self, items: List[Any], item_abspath: Callable = None) -> List[Any]:
        """Update path on items to be abspath."""
        item_abspath = item_abspath or self.item_abspath
        return [
            item.parse_obj(
                {
                    **item.dict(),
                    **{
                        "path": item_abspath(item.path),
                        "tentacles": [
                            inner_item.dict()
                            for inner_item in self.set_abspath(
                                cast(List, item.tentacles)
                            )
                        ],
                    },
                }
            )
            if hasattr(item, "path") and hasattr(item, "tentacles")
            else item.parse_obj(
                {
                    **item.dict(),
                    **{
                        "tentacles": [
                            inner_item.dict()
                            for inner_item in self.set_abspath(
                                cast(List, item.tentacles)
                            )
                        ]
                    },
                }
            )
            if hasattr(item, "tentacles")
            else item.parse_obj({**item.dict(), **{"path": item_abspath(item.path)}})
            if hasattr(item, "path")
            else item
            for item in items
        ]
