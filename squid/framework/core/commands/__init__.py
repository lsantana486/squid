from abc import ABCMeta, abstractmethod
from typing import Any, List


class Receiver(object, metaclass=ABCMeta):
    """Abstract receiver class as part of the Command pattern."""

    def action(self, name: str, *args, **kwargs) -> Any:
        """Delegates which method to be called for a desired action.

        Args:
            name: str
            args: list
            kwargs: dict

        Returns:
            Any

        Raises:
            AttributeError: Invalid Action.

        """
        try:
            return getattr(self, name)(*args, **kwargs)
        except AttributeError:
            raise AttributeError("Invalid Action.")


class Command(object, metaclass=ABCMeta):
    """Abstract Command class as part of the Command pattern."""

    def __init__(self, receiver: Receiver) -> None:
        """Initialize a new command instance.

        Args:
            receiver: Receiver

        """
        self._receiver = receiver

    @abstractmethod
    def execute(self) -> Any:
        """Abstract method for executing an action."""
        pass

    @abstractmethod
    def unexecute(self) -> Any:
        """Abstract method for unexecuting an action."""
        pass


class Invoker(object, metaclass=ABCMeta):
    """Abstract Invoker class as part of the Command pattern."""

    _history: List[Any]

    def __init__(self, valid_commands: List[Any]) -> None:
        """Initialize a new Invoker instance.

        Args:
            valid_commands: List[Any]

        """
        self._history = []
        self._valid_commands = valid_commands

    def execute(self, command: Any) -> Any:
        """Execute a command.

        Args:
            command: Any

        Returns:
            Any

        Raises:
            AttributeError: Invalid Command.

        """
        if command.__class__ not in self._valid_commands:
            raise AttributeError("Invalid Command")
        else:
            self._history.append(command)
            return command.execute()

    def undo(self, backwards: int = 0) -> Any:
        """Undo the last command.

        Args:
            backwards: int = 0

        Returns:
            Any

        """
        return self._history.pop(len(self._history) - 1 - backwards).unexecute()
