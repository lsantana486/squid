"""Command Update Tentacle|Tasks|Plugins."""
from typing import Any, Dict

from squid.framework.core.commands import Command
from squid.framework.core.commands.add import AddPlugins, AddTasks, AddTentacles
from squid.framework.core.commands.get import GetPlugins, GetTasks, GetTentacles
from squid.framework.core.models.bases import SquidTable


class UpdateTentacles(Command):
    """Command Update Tentacles."""

    __slots__ = ("query", "kvs", "_originals")

    def __init__(self, receiver: Any, query: str, kvs: Dict) -> None:
        """Constructor.

        Args:
            receiver: Any
            query: str
            kvs: Dict

        Raises:
            Exception: Path or Name cannot be modified after creation.

        """
        super().__init__(receiver)
        self.query = query
        if "path" in kvs or "name" in kvs:
            raise Exception("Path or Name cannot be modified after creation.")
        self.kvs = kvs
        self._originals = GetTentacles(receiver=receiver, query=query).execute()

    def execute(self) -> bool:
        """Execute Update Tentacles.

        Returns:
            bool

        """
        return self._receiver.action(
            "update", self.query, SquidTable.TENTACLES, self.kvs
        )

    def unexecute(self) -> bool:
        """Undo Execute Update Tentacles.

        Returns:
            bool

        """
        return AddTentacles(
            receiver=self._receiver, tentacles=self._originals, upsert=True
        ).execute()


class UpdateTasks(Command):
    """Command Update Tasks."""

    __slots__ = ("query", "kvs", "_originals")

    def __init__(self, receiver: Any, query: str, kvs: Dict) -> None:
        """Constructor.

        Args:
            receiver: Any
            query: str
            kvs: Dict

        Raises:
            Exception: Name cannot be modified after creation.

        """
        super().__init__(receiver)
        self.query = query
        if "name" in kvs:
            raise Exception("Name cannot be modified after creation.")
        self.kvs = kvs
        self._originals = GetTasks(receiver=receiver, query=query).execute()

    def execute(self) -> bool:
        """Execute Update Tasks.

        Returns:
            bool

        """
        return self._receiver.action("update", self.query, SquidTable.TASKS, self.kvs)

    def unexecute(self) -> bool:
        """Undo Execute Update Tasks.

        Returns:
            bool

        """
        return AddTasks(
            receiver=self._receiver, tasks=self._originals, upsert=True
        ).execute()


class UpdatePlugins(Command):
    """Command Update Plugins."""

    __slots__ = ("query", "kvs", "_originals")

    def __init__(self, receiver: Any, query: str, kvs: Dict) -> None:
        """Constructor.

        Args:
            receiver: Any
            query: str
            kvs: Dict

        Raises:
            Exception: Path or Name or Source cannot be modified after creation.

        """
        super().__init__(receiver)
        self.query = query
        if "name" in kvs or "path" in kvs or "source" in kvs:
            raise Exception("Path or Name or Source cannot be modified after creation.")
        self.kvs = kvs
        self._originals = GetPlugins(receiver=receiver, query=query).execute()

    def execute(self) -> bool:
        """Execute Update Plugins.

        Returns:
            bool

        """
        return self._receiver.action("update", self.query, SquidTable.PLUGINS, self.kvs)

    def unexecute(self) -> bool:
        """Undo Execute Update Plugins.

        Returns:
            bool

        """
        return AddPlugins(
            receiver=self._receiver, plugins=self._originals, upsert=True
        ).execute()
