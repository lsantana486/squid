"""Command Dump Tentacles|Tasks|Plugins."""
import os
from typing import Any, cast

from squid.framework.core.commands import Command
from squid.framework.core.commands.get import GetPlugin, GetTentacle, Plugin, Tentacle
from squid.framework.services.persistence import (
    PersistenceResponsePath,
    SquidPersistence,
)


class DumpTentacle(Command):
    """Command Dump Tentacle Config."""

    __slots__ = "tentacle"

    def __init__(self, receiver: Any, tentacle_name: str) -> None:
        """Constructor.

        Args:
            receiver: Any
            tentacle_name: str

        """
        super().__init__(receiver)
        self.tentacle = GetTentacle(receiver, tentacle_name, True).execute()

    def execute(self) -> None:
        """Execute DumpTentacle."""
        tentacle_path = os.path.abspath(
            os.path.join(
                cast(
                    PersistenceResponsePath, SquidPersistence().get_root_path().data
                ).path,
                cast(Tentacle, self.tentacle).path,
                ".tentacle",
            )
        )
        SquidPersistence.dump_to_file(
            item=cast(Tentacle, self.tentacle).dict(),
            path=tentacle_path,
            filename="config",
        )

    def unexecute(self) -> None:
        """Undo Execute Dump Tentacle Config."""
        try:
            os.remove(
                os.path.abspath(
                    os.path.join(
                        cast(
                            PersistenceResponsePath,
                            SquidPersistence().get_root_path().data,
                        ).path,
                        cast(Tentacle, self.tentacle).path,
                        ".tentacle",
                        "config.yaml",
                    )
                )
            )
        except FileNotFoundError as e:
            raise Exception(f"DumpTentacle -> undo: {e.strerror}")


class DumpPlugin(Command):
    """Command Dump Plugin Config."""

    __slots__ = "plugin"

    def __init__(self, receiver: Any, plugin_name: str) -> None:
        """Constructor.

        Args:
            receiver: Any
            plugin_name: str

        """
        super().__init__(receiver)
        self.plugin = GetPlugin(receiver, plugin_name).execute()

    def execute(self) -> None:
        """Execute DumpPlugin."""
        plugin_path = os.path.abspath(
            os.path.join(
                cast(
                    PersistenceResponsePath, SquidPersistence().get_root_path().data
                ).path,
                cast(Plugin, self.plugin).path,
                ".squid-plugin",
            )
        )
        SquidPersistence.dump_to_file(
            item=cast(Plugin, self.plugin).dict(), path=plugin_path, filename="config"
        )

    def unexecute(self) -> None:
        """Undo Execute Dump Plugin Config."""
        try:
            os.remove(
                os.path.abspath(
                    os.path.join(
                        cast(
                            PersistenceResponsePath,
                            SquidPersistence().get_root_path().data,
                        ).path,
                        cast(Plugin, self.plugin).path,
                        ".squid-plugin",
                        "config.yaml",
                    )
                )
            )
        except FileNotFoundError as e:
            raise Exception(f"DumpPlugin -> undo: {e.strerror}")
