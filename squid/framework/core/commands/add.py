"""Command Add Tentacle | Tasks | Plugins."""
from typing import Any, List, Tuple, Union

from squid.framework.core.commands import Command
from squid.framework.core.commands.dump import DumpPlugin, DumpTentacle
from squid.framework.core.models.bases import (
    Plugin,
    SquidTable,
    Task,
    TaskRef,
    Tentacle,
    TentacleRef,
)
from squid.framework.services.persistence import SquidPersistence
from squid.framework.utils import flatten_deep_item_to_ref_links


class AddTentacles(Command):
    """Command Add Tentacles."""

    __slots__ = ("tentacles", "upsert", "_squid_items", "_squid_links")

    def __init__(
        self,
        receiver: Any,
        tentacles: List[Union[Tentacle, TentacleRef]],
        upsert: bool = False,
    ) -> None:
        """Constructor.

        Args:
            receiver: Any
            tentacles: List[Union[Tentacle, TentacleRef]]
            upsert: bool = False

        """
        super().__init__(receiver)
        self.tentacles = tentacles
        self.upsert = upsert
        self._squid_items: List[Union[TaskRef, TentacleRef]] = []
        self._squid_links: List[
            Tuple[List[Union[TaskRef, TentacleRef]], List[Union[TaskRef, TentacleRef]]]
        ] = []

    def execute(self) -> bool:
        """Execute Add Tentacles.

        Returns:
            bool

        """
        self._squid_items, self._squid_links = flatten_deep_item_to_ref_links(self.tentacles)  # type: ignore
        for squid_item in self._squid_items:
            if isinstance(squid_item, TentacleRef):
                squid_item.path = SquidPersistence().make_tentacle_path(squid_item)
        resp_add = self._receiver.action("add", self._squid_items, self.upsert)
        for tentacle in self.tentacles:
            DumpTentacle(self._receiver, tentacle.name).execute()

        if self.upsert:
            for squid_item in self._squid_items:
                squid_item_cls = (
                    "tentacle" if isinstance(squid_item, TentacleRef) else "task"
                )
                self._receiver.action(
                    "update",
                    f"($.name == '{squid_item.name}') & ($.cls == '{squid_item_cls}')",
                    SquidTable.LINKS,
                    {"dependencies": []},
                )

        resp_link = [
            self._receiver.action("link", sources, targets)
            for (sources, targets) in self._squid_links
        ]
        return all([resp_add, *resp_link])

    def unexecute(self) -> bool:
        """Undo Execute Add Tentacles.

        Returns:
            bool

        """
        squid_items_to_remove = []
        for squid_item in self._squid_items:
            squid_items_to_remove.append(
                (
                    f"$.name == '{squid_item.name}'",
                    SquidTable.TENTACLES
                    if isinstance(squid_item, TentacleRef)
                    else SquidTable.TASKS,
                )
            )
            if isinstance(squid_item, TentacleRef):
                SquidPersistence().remove_tentacle_path(squid_item)

        resp_remove = self._receiver.action("remove", squid_items_to_remove)
        resp_unlink = [
            self._receiver.action("unlink", sources, targets)
            for (sources, targets) in self._squid_links
        ]
        return all([resp_remove, *resp_unlink])


class AddTasks(Command):
    """Command Add Tasks."""

    __slots__ = ("tasks", "upsert", "_squid_items", "_squid_links")

    def __init__(
        self, receiver: Any, tasks: List[Union[Task, TaskRef]], upsert: bool = False,
    ) -> None:
        """Constructor.

        Args:
            receiver: Any
            tasks: List[Union[Task, TaskRef]]
            upsert: bool = False

        """
        super().__init__(receiver)
        self.tasks = tasks
        self.upsert = upsert
        self._squid_items: List[Union[TaskRef, TentacleRef]] = []
        self._squid_links: List[
            Tuple[List[Union[TaskRef, TentacleRef]], List[Union[TaskRef, TentacleRef]]]
        ] = []

    def execute(self) -> bool:
        """Execute Add Tasks.

        Returns:
            bool

        """
        self._squid_items, self._squid_links = flatten_deep_item_to_ref_links(self.tasks)  # type: ignore
        resp_add = self._receiver.action("add", self._squid_items, self.upsert)
        resp_link = [
            self._receiver.action("link", sources, targets)
            for (sources, targets) in self._squid_links
        ]
        return all([resp_add, *resp_link])

    def unexecute(self) -> bool:
        """Undo Execute Add Tasks.

        Returns:
            bool

        """
        squid_items_to_remove = []
        for squid_item in self._squid_items:
            squid_items_to_remove.append(
                (
                    f"$.name == '{squid_item.name}'",
                    SquidTable.TENTACLES
                    if isinstance(squid_item, TentacleRef)
                    else SquidTable.TASKS,
                )
            )

        resp_remove = self._receiver.action("remove", squid_items_to_remove)
        resp_unlink = [
            self._receiver.action("unlink", sources, targets)
            for (sources, targets) in self._squid_links
        ]
        return all([resp_remove, *resp_unlink])


class AddPlugins(Command):
    """Command Add Plugins."""

    __slots__ = ("plugins", "upsert")

    def __init__(
        self, receiver: Any, plugins: List[Plugin], upsert: bool = False,
    ) -> None:
        """Constructor.

        Args:
            receiver: Any
            plugins: List[Plugin]
            upsert: bool = False

        """
        super().__init__(receiver)
        self.plugins = plugins
        self.upsert = upsert

    def execute(self) -> bool:
        """Execute Add Plugins.

        Returns:
            bool

        """
        plugins = [
            Plugin.parse_obj(
                {
                    **plugin.dict(),
                    **{"path": SquidPersistence().make_squid_plugin_path(plugin)},
                }
            )
            for plugin in self.plugins
        ]
        resp = self._receiver.action("add", plugins, self.upsert)
        for plugin in plugins:
            DumpPlugin(self._receiver, plugin.name).execute()
        return resp

    def unexecute(self) -> bool:
        """Undo Execute Add Plugins.

        Returns:
            bool

        """
        return self._receiver.action(
            "remove",
            [
                (f"$.name == '{plugin.name}'", SquidTable.PLUGINS)
                for plugin in self.plugins
            ],
        )
