import json
from squid.framework.core import (
    Head,
    Squid,
    GetTentacles,
)


def inner_exec(tentacle: str = None) -> str:
    """Test for inner execution."""
    squid = Squid()
    squid_head = Head()

    return json.dumps(
        [
            tentacle.dict()
            for tentacle in squid_head.execute(
                GetTentacles(
                    squid,
                    "$.name.matches('.*')"
                    if tentacle is None
                    else f"$.name == '{tentacle}'",
                    "tree",
                )
            )
        ]
    )


def setup(registry):
    registry.entry("inner-exec", inner_exec)


if __name__ == "__main__":
    print(inner_exec())
