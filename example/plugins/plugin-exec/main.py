from squid.framework.core import Head, Squid, InvokeFn


def plugin_exec() -> str:
    """Test for inner execution."""
    squid = Squid()
    squid_head = Head()
    return squid_head.execute(InvokeFn(squid, "inner-exec"))()


def setup(registry):
    registry.entry("plugin-exec", plugin_exec)


if __name__ == "__main__":
    print(plugin_exec())
