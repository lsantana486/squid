"""Update local dependencies full paths according to user filesystem."""

import fileinput
import glob
import os
import re
import toml
from typing import Callable
import squid.framework.services.persistence

PROJECT_NAME: str = os.environ.get("CI_PROJECT_NAME", "squid-ci-playground")
REGEX = r"^url\s?=\s?\"(.*[^\"])\""

transform_path: Callable[[str, str], str] = lambda base_path, path: "/".join(
    base_path.split("/")[0 : base_path.split("/").index(PROJECT_NAME)]
    + path.split("/")[path.split("/").index(PROJECT_NAME) :]
)


def transform_local_dependencies_path(project_path: str) -> dict:
    with open(os.path.join(project_path, "pyproject.toml")) as f:
        toml_data: dict = toml.load(f)

    parser_obj = lambda obj: {
        **obj,
        "path": transform_path(project_path, obj.get("path", project_path)),
    }

    if (
        "tool" in toml_data
        and "poetry" in toml_data["tool"]
        and "dependencies" in toml_data["tool"]["poetry"]
    ):
        toml_data["tool"]["poetry"]["dependencies"] = {
            k: parser_obj(v) if isinstance(v, dict) else v
            for k, v in toml_data.get("tool", {})
            .get("poetry", {})
            .get("dependencies", {})
            .items()
        }
    return toml_data


def transform_local_dependencies_lock(project_path: str) -> None:
    poetry_lock_realpath = os.path.join(project_path, "poetry.lock")
    if os.path.exists(poetry_lock_realpath):
        for line in fileinput.input(poetry_lock_realpath, inplace=True):
            matches = re.finditer(REGEX, line)
            groups = [match.groups() for match in matches]
            if groups:
                new_path = transform_path(project_path, groups[0][0])
                line = line.replace(groups[0][0], new_path)
            print(line, end="")


def update_dp() -> str:
    """Update local dependencies full paths according to user filesystem."""
    root_path = (
        squid.framework.services.persistence.SquidPersistence()
        .get_root_path()
        .data.path
    )
    search_path = f"{root_path}/**/pyproject.toml"
    results = "Paths walked."
    for pyproject_toml_path in glob.iglob(search_path, recursive=True):
        project_realpath: str = os.path.dirname(os.path.realpath(pyproject_toml_path))
        toml_data = transform_local_dependencies_path(project_realpath)
        with open(os.path.join(project_realpath, "pyproject.toml"), "w") as f:
            toml.dump(toml_data, f)
        transform_local_dependencies_lock(project_realpath)
        results = results + f"\nupdate_dp -> DONE: {project_realpath}"
    return results


def setup(registry):
    registry.entry("update-dp", update_dp)


if __name__ == "__main__":
    update_dp()
