import json
from squid.framework.core import (
    Head,
    Squid,
    Execute,
    GetTasks,
    GetTentacles,
    InvokeCtx,
    InvokeFn,
    GetRegistryEntries,
)

squid = Squid()
squid_head = Head()

# squid_head.execute(
#     Execute(
#         squid,
#         squid_head.execute(GetTentacles(squid, "$.name == 'tentacle_a'", "tree")),
#         [],
#     )
# )
# squid_head.execute(
#     Execute(
#         squid,
#         squid_head.execute(GetTentacles(squid, "$.name == 'tentacle_b'", "tree")),
#         [],
#     )
# )
output = json.dumps(
    [
        tentacle.dict()
        for tentacle in squid_head.execute(
            GetTentacles(squid, "$.name.matches('.*')", "tree")
        )
    ]
)
# squid_head.execute(GetRegistryEntries(squid))
# squid_head.execute(InvokeFn(squid, "to_list"))({"a": 1, "b": 2})
#
# with squid_head.execute(InvokeCtx(squid, "to_list")):
#     from squid.framework.services.registry.plugins import plugin  # type: ignore
#
#     plugin.fn_iter({"a": 1, "b": 2})
#
# squid_head.execute(
#     Execute(
#         squid,
#         squid_head.execute(GetTentacles(squid, "$.name == 'tentacle_b'", "tree")),
#         squid_head.execute(GetTasks(squid, "$.name == 'task_c'", "tree")),
#     )
# )

print(output)
